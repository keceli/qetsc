FFLAGS     = -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace -ffpe-trap=zero,overflow,underflow
.DEFAULT_GOAL := qetsc
include ${SLEPC_DIR}/lib/slepc/conf/slepc_common

EXEFILE = qetsc
DEMOEXE = demo
NP      = 4
MATDIR  = ./matdir
qetsc: m_qetsc_tools.o m_qetsc.o test_qetsc.o chkopts
	-${FLINKER} -o ${EXEFILE} m_qetsc_tools.o m_qetsc.o test_qetsc.o ${SLEPC_EPS_LIB}
	${RM} m_qetsc_tools.o m_qetsc_tools.mod m_qetsc.o test_qetsc.o m_qetsc.mod

demo: m_qetsc_tools.o demo.o chkopts
	-${FLINKER} -o ${DEMOEXE} m_qetsc_tools.o demo.o 
	${RM} m_qetsc_tools.o m_qetsc_tools.mod demo.o 
#------------------------------------------------------------------------------------
test_mat: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 1 -matfile1 ${MATDIR}/matH.bin -matfile2 ${MATDIR}/matS.bin
	
test_p1_1: qetsc
	${MPIEXEC} -n 1 ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 0 -subtype 0 

test_p1_2: qetsc
	${MPIEXEC} -n 1 ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 1 -unbound 0 -subtype 0 

test_p1_3: qetsc
	${MPIEXEC} -n 1 ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 1 -subtype 0 

test_p1_4: qetsc
	${MPIEXEC} -n 1 ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 0 -subtype 7

test_p1_5: qetsc
	${MPIEXEC} -n 1 ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 1 -unbound 1 -subtype 7 

test_np_1: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 0 -subtype 0 

test_np_2: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 1 -unbound 0 -subtype 0 

test_np_3: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 1 -subtype 0 

test_np_4: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 0 -unbound 0 -subtype 7

test_np_5: qetsc
	${MPIEXEC} -n ${NP} ./${EXEFILE} -log_view -eps_interval 0.5,15 -nreq 10 -inertia 1 -unbound 1 -subtype 7 

test_all: test_np_1 test_np_2 test_np_3 test_np_4 test_np_5 \
	test_p1_1 test_p1_2 test_p1_3 test_p1_4 test_p1_5 
